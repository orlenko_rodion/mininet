sentTime=$(($(date +%s%N)/1000000))

curl -X POST http://$1 \
  -H "Content-Type: application/json" \
  -H "sent-time: $sentTime" \
  -H "id: $3" \
  -d  @$2 \
