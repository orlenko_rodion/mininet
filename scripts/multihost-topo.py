from mininet.topo import Topo  
class MyTopo( Topo ):
    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        switch1 = self.addSwitch('s1')
        host11 = self.addHost('h1-1')
        host12 = self.addHost('h1-2')
        host13 = self.addHost('h1-3')

        switch2 = self.addSwitch('s2')
        host21 = self.addHost('h2-1')
        host22 = self.addHost('h2-2')
        host23 = self.addHost('h2-3')

        switch3 = self.addSwitch('s3')
        host31 = self.addHost('h3-1')
        host32 = self.addHost('h3-2')
        host33 = self.addHost('h3-3')

        # Add links
        self.addLink(host11, switch1)
        self.addLink(host12, switch1)
        self.addLink(host13, switch1)

        self.addLink(host21, switch2)
        self.addLink(host22, switch2)
        self.addLink(host23, switch2)

        self.addLink(host31, switch3)
        self.addLink(host32, switch3)
        self.addLink(host33, switch3)

        self.addLink(switch1, switch2)
        self.addLink(switch2, switch3)


topos = {'mytopo': (lambda: MyTopo())}
