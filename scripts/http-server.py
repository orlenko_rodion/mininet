#!/usr/bin/python
import time

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

PORT_NUMBER = 8080
MAX_REQUESTS = 15


# This class will handles any incoming request from
class myHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        self.send_response(200)
        # self.send_header('Content-type', 'text/html')
        self.end_headers()
        now = int(round(time.time() * 1000))
        spentTime = now - int(self.headers.getheader('sent-time'))
        id = self.headers.getheader('id')
        print(id + " " + str(spentTime))
        self.wfile.write("")
        return


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT_NUMBER), myHandler)
    # print('Started httpserver on port ', PORT_NUMBER)

    # Wait forever for incoming http requests
    server.serve_forever()

except KeyboardInterrupt:
    # print('\n shutting down the web server')
    server.socket.close()