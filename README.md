
## Precondition
installed:
* python
* mn 
* xterm
* curl 

### Mininet topology
```html
         co
         
    s1 - s2 - s3
    |     |     |
 h1-1   h2-1    h3-1
 h1-2   h2-2    h3-2
 h1-3   h2-3    h3-3
 ```
### work stages
1) deploy custom mininet network 
2) xterm to edge hosts(h1-1, h3-3)
3) setup server to consume http requests on h3-3 
4) setup client to emulate huge http traffic through whole network 
5) process data using 'gnuplot' and get graphics 

**configuring server and client on the edge of network**
![Screenshot](resources/img/client-server-config.png)

#### PROC 
![Screenshot](resources/img/proc-report.png)

#### CFS
![Screenshot](resources/img/cfs-report.png)

### Util commands
**Start mininet custom topology**
```commandline
sudo mn --custom ./scripts/multihost-topo.py --topo=mytopo
sudo mn --custom ./scripts/multihost-topo.py --topo=mytopo --host proc
sudo mn --custom ./scripts/multihost-topo.py --topo=mytopo --host cfs
```

**Run http server and collect statistics**

```commandline
python http-server.py > output.txt
``` 

**Send single POST request with json file**
```commandline
./sendJsonFilePostRequest.sh localhost:8080 ./resources/citylots.json 2
```

**Emulate huge http traffic**
send to localhost 5 POST requests
```commandline
./httpTraficEmulator.sh localhost:8080 5 ./resources/citylots.json
```
